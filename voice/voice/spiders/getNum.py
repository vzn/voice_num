# -*- coding: utf-8 -*-
import scrapy
import pymysql
import time
import json
import urllib.parse as urlparse
from scrapy.http import Request
from ..items import VoiceItem

from scrapy.http.cookies import CookieJar


class GetnumSpider(scrapy.Spider):
    name = 'getNum'
    allowed_domains = ['google.com']
    start_urls = ['http://google.com/']
    cookie_dict = {}
    # code all
    with open('/Users/hyhnm/GitHub/voice_num/voice/voice/data.json', 'r') as f:
        code = json.loads(f.read())
    for k, v in code.items():
        code[k] = 0

    def start_requests(self):
        url = 'https://www.google.com/voice/b/0#inbox'
        cookie = 'NID=112=DRuqi301P1eIT2JxnUGIP3dKToKPPoR8qseE1DhebX-PZfpUmPZ9svN_a_jUncvrh192qIoIG4PgbaiKD-YPgdYaMzkuPV4E9E4vsKP5gNQrAY95dFJQJgDhnstdeASxARYJqCKzvuxqzPxWWurSN2F8rPBagxZ_YPdc9ARDrOjfNOMrAEVmg0f7a8PUGMPgMxpgiO-SSRAc_tM7jd8BbI-KqdEd4aoCbewBsfHMieo3YfQ; CONSENT=YES+CN.zh-CN+20160710-07-0; 1P_JAR=2017-9-14-13; S=billing-ui-v3=0_3u8ZPGHTXcRScOQCIiWScWCd625st5:billing-ui-v3-efe=0_3u8ZPGHTXcRScOQCIiWScWCd625st5:grandcentral=RYYkWh4L0OE6NkeLCGINLZ5n20lVEFyk; SID=KgWzN8uTHEqDrIHuQus8k1Qq5lWu5kkgYbJDxqOUscgg_8bpSibfrRs2dh6WO4s1Cwfy3w.; HSID=Ax6oJxKTlBP-yXxBb; SSID=AwYg--5UM8dhzfKwr; APISID=VPbxT7LqkVhK__B5/A-qsuchzcJTQuJeFa; SAPISID=-IlaHcaabiEjVY3H/A5VC-PlIoI7fFA8N9'
        tmp = cookie.split(';')
        # 自动设置 cookie 键值对
        for i in tmp:
            k, v = i.split('=', 1)
            self.cookie_dict[k] = v
        # print(v,type(v))

        print(0)
        yield Request(url=url, cookies=self.cookie_dict, callback=self.get_home)  # 执行回调函数

    def get_home(self, response):
        print(1)
        # url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=336&start=0&country=US'
        #
        # yield Request(url=url, cookies=self.cookie_dict, callback=self.get_num)
        # print(v)
        # https://www.google.com/voice/b/0/setup/searchnew/?ac=612&start=0&country=US
        cookie = self.cookie(response)
        n = 0
        # from concurrent.futures import ThreadPoolExecutor
        # p = ThreadPoolExecutor(20)
        for item in self.code.keys():
            n += 1
            print('code ++++++++++++++>>>>>>> %s' % n)
            # url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=%s&start=0&country=US' % (i)
            url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=%s&start=0&country=US' % (item)
            # time.sleep(1)
            print(item, url, '发了多少次请求:%s' % (n))
            yield Request(url=url, cookies=self.cookie_dict, callback=self.get_num)
            # p.submit(Request,url=url, cookies=self.cookie_dict, callback=self.get_num)
            #     # print(response.text)

    def get_num(self, response):
        print('+++++++++++++ Play get_num ++++++++++++++')
        result_tmp = urlparse.urlparse(response.url)
        print('result_tmp:%s' % json.dumps(result_tmp))
        result = urlparse.parse_qs(result_tmp.query)
        print('result:%s' % json.dumps(result))
        response_dict = json.loads(response.text)
        yield VoiceItem(json=response_dict['JSON'], code=result['ac'][0])
        '''
        v = {'JSON': {'num_matches': '88', 'translated_query': '',
              'vanity_info': {'+13362236184': 0, '+13362836187': 0, '+13362981632': 0, '+13362982042': 0,
                              '+13362982440': 0}}}
        '''
        ac = result['ac'][0]
        # n = 0
        num_matches = int(response_dict['JSON']['num_matches'])
        # url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac={0}&start={1}&country=US'
        # while self.code[ac] < num_matches or len(response_dict['JSON']['vanity_info']) == 5:
        if len(response_dict['JSON']['vanity_info']) != 0:
            if num_matches == 500:
                self.code[ac] = self.code[ac] + 5
                url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=%s&start=%s&country=US' % (ac, self.code[ac])
                print('url:%s' % url)
                yield Request(url=url, cookies=self.cookie(response), callback=self.process_500)
            else:
                while self.code[ac] < num_matches:
                    # url = url.format(result['ac'][0],n)
                    self.code[ac] = self.code[ac] + 5
                    url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=%s&start=%s&country=US' % (
                    ac, self.code[ac])
                    print('url:%s' % url)
                    yield Request(url=url, cookies=self.cookie(response), callback=self.get_num)

    def process_500(self, response):
        response_dict = json.loads(response.text)
        result_tmp = urlparse.urlparse(response.url)
        result = urlparse.parse_qs(result_tmp.query)
        yield VoiceItem(json=response_dict['JSON'], code=result['ac'][0])
        ac = result['ac'][0]
        if len(response_dict['JSON']['vanity_info']) == 5:
            self.code[ac] = self.code[ac] + 5
            url = 'https://www.google.com/voice/b/0/setup/searchnew/?ac=%s&start=%s&country=US' % (ac, self.code[ac])
            print('url:%s' % url)
            yield Request(url=url, cookies=self.cookie(response), callback=self.process_500)

    def process_data(self, response):
        '''
        处理每个区号的响应结果
        :param response:
        :return:
        '''
        print('>>>>>>>>>>>>>> Play Process  <<<<<<<<<<<<<<<')
        result_tmp = urlparse.urlparse(response.url)
        result = urlparse.parse_qs(result_tmp.query)
        response_dict = json.loads(response.text)
        if len(response_dict['JSON']['vanity_info']) != 0:
            print('response:', response)
            yield VoiceItem(json=response_dict['JSON'], code=result['ac'][0])
        print('<<<<<<<<<<<<<<<<  END Process  >>>>>>>>>>>>>>>>')

    # def line_num(self,code,vanity_info,json):
    # def line_num(self, json, code):
    #     '''
    #     处理成数据行
    #     :param json:
    #     :param code:
    #     :return:
    #     '''
    #     print('line_num -------------------')
    #     # vanity_info = json['vanity_info']
    #     # print('vanity_info:',vanity_info)
    #     # for k, v in vanity_info.items():
    #     yield VoiceItem(code=code, json=json)

    def cookie(self, response):
        '''
        cookie
        :param response:
        :return:
        '''
        cookie_jar = CookieJar()  # 对象 里面封装了cookie
        cookie_jar.extract_cookies(response, response.request)  # 去响应中提取cookie
        return cookie_jar
