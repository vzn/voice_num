# -*- coding: utf-8 -*-
import pymysql
import json


# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


# class JsonPipeline(object):
#     def process_item(self, item, spider):
#         pass
#     @classmethod
#     def from_crawler(cls, crawler):
#         pass
#     def open_spider(self, spider):
#         pass
#     def close_spider(self, spider):
#         pass

class VoicePipeline(object):
    host = None
    user = None
    pwd = None
    database = None
    port = None
    coding = None
    conn = None
    cursor = None
    code = None
    count = 0
    quantity = 0

    def process_item(self, item, spider):
        # self.cursor = self.conn.cursor()
        # raw = scrapy.Field()  # +13362236184
        # formatted = scrapy.Field()  #
        # code = scrapy.Field()  # 612 > 外键
        # start = scrapy.Field()  # 0
        print('--------  号码数量: %s  执行次数:%s -------'%(self.count,self.quantity))
        print('item:',item)
        if len(item['json']['vanity_info']) != 0:
            vanity_info = item['json']['vanity_info']
            for k, v in vanity_info.items():
                self.count += 1
                formatted = ''.join((('(',k[2:5],')',' ',k[5:8],'-',k[-4:])))
                data=[self.code[item['code']],k,formatted,k[-10:],str(v)]
                print('data:',data)
                sql = 'insert into num(ac,raw,formatted,last_num,start) values(%s,%s,%s,%s,%s)'
                self.cursor.execute(sql,(data))
            data_dict = item['json']
            sql_json = 'insert into json_data(num_matches,translated_query,vanity_info,raw) VALUES(%s,%s,%s,%s)'
            print('data_dict:',data_dict)
            sql_json_data = [data_dict['num_matches'],data_dict['translated_query'],json.dumps(data_dict['vanity_info']),json.dumps(data_dict)]
            print('sql_json_data:',sql_json_data)
            self.quantity+=1
            # print(self.cursor._query())
            self.cursor.execute(sql_json,(sql_json_data))
            self.conn.commit()

    @classmethod
    def from_crawler(cls, crawler):
        """
        初始化时候，用于创建pipeline对象
        :param crawler:
        :return:
        """
        cls.host = crawler.settings.get('HOST')  # 去配置文件读取   可以是 mysql 连接数据
        cls.user = crawler.settings.get('USER')  # 去配置文件读取   可以是 mysql 连接数据
        cls.pwd = crawler.settings.get('PASSWORD')  # 去配置文件读取   可以是 mysql 连接数据
        cls.database = crawler.settings.get('DATABASE')  # 去配置文件读取   可以是 mysql 连接数据
        cls.port = crawler.settings.get('PORT')  # 去配置文件读取   可以是 mysql 连接数据
        cls.coding = crawler.settings.get('CODING')  # 去配置文件读取   可以是 mysql 连接数据


        # return cls(val)
        return cls()

    def open_spider(self, spider):
        """
        爬虫开始执行时，调用
        :param spider:
        :return:
        """
        self.conn = pymysql.connect(host=self.host, user=self.user, password=self.pwd, database=self.database,
                                    port=self.port, charset=self.coding)
        self.cursor = self.conn.cursor()
        with open('/Users/hyhnm/GitHub/voice_num/voice/voice/data.json', 'r') as f:
            self.code = json.loads(f.read())

        # test 测试
        # sql = 'select id,area_code from code where state_id is NOT NULL'
        # self.cursor.execute(sql)
        # v = self.cursor.fetchall()
        # data = {}
        # for i in v:
        #     print(i)
        #     data[i[1]] = i[0]
        # with open('data.json', 'w') as f:
        #     f.write(json.dumps(data))

        print('数据库连接开启')

    def close_spider(self, spider):
        """
        爬虫关闭时，被调用
        :param spider:
        :return:
        """
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
        print('数据库连接关闭')
